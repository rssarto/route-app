package com.goeuro.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.goeuro.controller.BusRouteController;
import com.goeuro.data.BusRouteFileResource;
import com.goeuro.logging.RouteServiceLogger;
import com.goeuro.serviceImpl.BusRouteServiceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SpringBootApplication
@ComponentScan(basePackageClasses={BusRouteController.class, BusRouteServiceImpl.class, BusRouteFileResource.class})
@EnableAspectJAutoProxy
public class RouteApplication {
	
	public static String BUS_ROUTE_DATA_FILE = "./src/data/example";
	public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

	public static void main(String[] args) {
		if( args != null && args.length > 0 ){
			BUS_ROUTE_DATA_FILE = args[0];
		}
		SpringApplication.run(RouteApplication.class, args);
	}
	
	@Bean
	public RouteServiceLogger routeServiceLogger(){
		return new RouteServiceLogger();
	}

}
