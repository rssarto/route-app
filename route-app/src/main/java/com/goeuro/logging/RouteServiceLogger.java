package com.goeuro.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class RouteServiceLogger {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Around("execution(* com.goeuro.service.RouteService.existRouteBetweenStations(..)) and args(firstStation,secondStation)")
	public Object logExistRouteBetweenStation(ProceedingJoinPoint pjp, Integer firstStation, Integer secondStation) throws Throwable{
		log.info("departureId: " + firstStation + " - arrivalId: " + secondStation);
		log.info(pjp.proceed().toString() );
		return pjp.proceed();
	}
	
	
}
