package com.goeuro.service;

import com.goeuro.common.AbstractSearchResult;

public interface RouteService {
	
	AbstractSearchResult existRouteBetweenStations(Integer firstStation, Integer secondStation);

}
