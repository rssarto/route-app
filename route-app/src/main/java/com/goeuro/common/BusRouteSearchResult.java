package com.goeuro.common;

import com.goeuro.application.RouteApplication;

public class BusRouteSearchResult extends AbstractSearchResult {
	

	private boolean direct_bus_route;

	public BusRouteSearchResult(int dep_sid, int arr_sid, boolean direct_bus_route) {
		super(dep_sid,arr_sid);
		this.direct_bus_route = direct_bus_route;
	}
	
	public boolean isDirect_bus_route() {
		return direct_bus_route;
	}
	public void setDirect_bus_route(boolean direct_bus_route) {
		this.direct_bus_route = direct_bus_route;
	}
	
	@Override
	public boolean result() {
		return isDirect_bus_route();
	}
	
	@Override
	public int departureId() {
		return this.getDep_sid();
	}

	@Override
	public int arrivalId() {
		return this.getArr_sid();
	}

	@Override
	public String toString() {
		return RouteApplication.gson.toJson(this);
	}
	
}
