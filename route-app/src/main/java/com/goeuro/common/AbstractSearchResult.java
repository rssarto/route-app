package com.goeuro.common;

public abstract class AbstractSearchResult {
	
	private int dep_sid;
	private int arr_sid;
	
	public AbstractSearchResult(int dep_sid, int arr_sid) {
		this.dep_sid = dep_sid;
		this.arr_sid = arr_sid;
	}
	
	public int getDep_sid() {
		return dep_sid;
	}
	public void setDep_sid(int dep_sid) {
		this.dep_sid = dep_sid;
	}
	public int getArr_sid() {
		return arr_sid;
	}
	public void setArr_sid(int arr_sid) {
		this.arr_sid = arr_sid;
	}
	
	public abstract boolean result();
	public abstract int departureId();
	public abstract int arrivalId();

}
