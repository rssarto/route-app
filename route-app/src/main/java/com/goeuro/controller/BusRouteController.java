package com.goeuro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goeuro.annotations.Bus;
import com.goeuro.annotations.Route;
import com.goeuro.common.AbstractSearchResult;
import com.goeuro.service.RouteService;

@RestController
public class BusRouteController {
	
	@Autowired
	@Route
	@Bus
	private RouteService routeService;
	
	@RequestMapping(value="/direct", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractSearchResult> direct(@RequestParam(name="dep_sid", required=true) int departureId, @RequestParam(value="arr_sid", required=true) int arrivalId){
		ResponseEntity<AbstractSearchResult> result = new ResponseEntity<AbstractSearchResult>(routeService.existRouteBetweenStations(departureId, arrivalId), HttpStatus.OK);
		return result;
	}

}
