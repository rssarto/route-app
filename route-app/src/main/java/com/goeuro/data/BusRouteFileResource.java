package com.goeuro.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.goeuro.application.RouteApplication;

@Component
public class BusRouteFileResource {
	
	private Map<Integer, List<Integer>> busRoutes = new HashMap<>();
	
	public BusRouteFileResource() throws IOException{
		Path file = Paths.get(RouteApplication.BUS_ROUTE_DATA_FILE);
		try(InputStream in = Files.newInputStream(file)){
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			
			line = reader.readLine();
			busRoutes = new HashMap<>(new Integer(line));
			
			while((line = reader.readLine()) != null){
				List<String> routeAsList = Arrays.asList(line.split(" "));
				List<Integer> routes = new ArrayList<>();
				
				routeAsList.subList(1, routeAsList.size()).forEach((s) -> {routes.add(new Integer(s));});
				busRoutes.put(new Integer(routeAsList.get(0)), routes);
			}
		}catch(IOException ex){
			throw ex;
		}		
	}

	public Map<Integer, List<Integer>> getBusRoutes() {
		return busRoutes;
	}

}
