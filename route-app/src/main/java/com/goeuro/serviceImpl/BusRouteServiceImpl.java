package com.goeuro.serviceImpl;

import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.goeuro.annotations.Bus;
import com.goeuro.annotations.Route;
import com.goeuro.common.AbstractSearchResult;
import com.goeuro.common.BusRouteSearchResult;
import com.goeuro.data.BusRouteFileResource;
import com.goeuro.service.RouteService;

@Service
@Route
@Bus
public class BusRouteServiceImpl implements RouteService {
	
	@Autowired
	BusRouteFileResource busRouteFileResource;
	
	@Override
	public AbstractSearchResult existRouteBetweenStations(Integer firstStation, Integer secondStation) {
		BusRouteSearchResult searchResult = new BusRouteSearchResult(firstStation, secondStation, false);
		
		for( Entry<Integer, List<Integer>> entry : busRouteFileResource.getBusRoutes().entrySet() ){
			List<Integer> routes = entry.getValue();
			if( routes.contains(firstStation)  && routes.contains(secondStation)){
				if( routes.indexOf(secondStation) > routes.indexOf(firstStation) ){
					searchResult.setDirect_bus_route(true);
					break;
				}
			}
			
		}
		
		return searchResult;
	}
}
