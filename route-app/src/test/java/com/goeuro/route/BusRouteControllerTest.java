package com.goeuro.route;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.goeuro.annotations.Bus;
import com.goeuro.annotations.Route;
import com.goeuro.application.RouteApplication;
import com.goeuro.controller.BusRouteController;
import com.goeuro.service.RouteService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers=BusRouteController.class)
@ContextConfiguration(classes=RouteApplication.class) 
public class BusRouteControllerTest {
	
	@Autowired
	@Route
	@Bus
	RouteService busRouteService;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void findExistingRoute() throws Exception{
		this.mvc.perform(get("/direct?dep_sid=142&arr_sid=12"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
			.andExpect(jsonPath("$.*", hasSize(3)))
    		.andExpect(jsonPath("$.dep_sid", equalTo(142)))
    		.andExpect(jsonPath("$.arr_sid", equalTo(12)))
    		.andExpect(jsonPath("$.direct_bus_route", equalTo(true)));
	}
	
	@Test
	public void findNotExistingRoute() throws Exception{
		this.mvc.perform(get("/direct?dep_sid=14&arr_sid=12"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
			.andExpect(jsonPath("$.*", hasSize(3)))
    		.andExpect(jsonPath("$.dep_sid", equalTo(14)))
    		.andExpect(jsonPath("$.arr_sid", equalTo(12)))
    		.andExpect(jsonPath("$.direct_bus_route", equalTo(false)));
	}
	
	@Test
	public void findWithNoArrivalId() throws Exception{
		this.mvc.perform(get("/direct?dep_sid=14"))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void findWithNoDeparturelId() throws Exception{
		this.mvc.perform(get("/direct?arr_sid=12"))
		.andExpect(status().isBadRequest());
	}
}
