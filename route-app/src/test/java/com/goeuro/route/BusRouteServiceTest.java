package com.goeuro.route;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.goeuro.annotations.Bus;
import com.goeuro.annotations.Route;
import com.goeuro.application.RouteApplication;
import com.goeuro.service.RouteService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=RouteApplication.class) 
public class BusRouteServiceTest {
	
	@Autowired
	@Route
	@Bus
	RouteService busRouteService;
	
	@Test
	public void loadBusRoute(){
		Assert.assertNotNull(busRouteService);
	}
	
	@Test
	public void checkAvaiableConnectionsRoute(){
		Assert.assertTrue(busRouteService.existRouteBetweenStations(153, 150).result());
	}
	
	@Test
	public void checkUnavailableConnectionsRoute(){
		Assert.assertFalse(busRouteService.existRouteBetweenStations(1, 2).result());
	}

}
